<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PhDHub
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'phdhub' ); ?></a>

	<header id="masthead" class="site-header">
		<div id="top-header">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					<div class="local-hubs-menu" data-uk-dropdown>
						<div><?php echo __('All Hubs', 'phdhub'); ?></div>
						<div class="uk-dropdown">
							<?php
								$subsites = get_sites();
								if ( ! empty ( $subsites ) ) {
									echo '<ul class="subsites">';
									foreach( $subsites as $subsite ) {
										$subsite_id = get_object_vars( $subsite )["blog_id"];
										$subsite_name = get_blog_details( $subsite_id )->blogname;
										$subsite_link = get_blog_details( $subsite_id )->siteurl;
										echo '<li class="site-' . $subsite_id . '"><a href="' . $subsite_link . '">' . $subsite_name . '</a></li>';
									}
									echo '</ul>';
								}
							?>
						</div>
					</div>
					<?php
						if ( is_active_sidebar('language-switcher') ) {
					?>
					<a class="language-switcher-link" href="#languages-list" data-uk-modal>
						<span class="fa fa-globe" aria-hidden="true"></span> <?php echo __('Select your language', 'phdhub'); ?>
					</a>
					<div id="languages-list" class="uk-modal">
						<div class="uk-modal-dialog">
							<p class="modal-title">
								<?php echo __('Select your language', 'phdhub'); ?>
								<a class="uk-modal-close uk-close"></a>
							</p>
							<?php
								dynamic_sidebar('language-switcher');
							?>
						</div>
					</div>
					<?php
						}
					?>
				</div>
				<div class="uk-width-1-2">
					<?php
						if ( is_active_sidebar( 'user-area' ) ) {
							dynamic_sidebar( 'user-area' );
						}
					?>
					<div class="mobile-nav uk-visible-small">
						<a href="#mobile-menu" data-uk-offcanvas><span class="fa fa-bars" aria-hidden="true" title="Menu"></span><span class="uk-hidden"><?php echo __('Menu'); ?></span></a>
					</div>
					<div id="mobile-menu" class="uk-offcanvas">
						<div class="uk-offcanvas-bar">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							) );
							
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-grid">
			<div class="uk-width-3-10">
				<div class="site-branding">
					<h1 class="site-title">
						<a href="<?php echo esc_url( site_url( '/' ) ); ?>">
							<?php
								if ( has_custom_logo() ) {
									the_custom_logo();
								}
								else {
							?>
							<img src="<?php echo get_template_directory_uri() . '/images/phdhub.png'; ?>" alt="PhD Hub">
							<?php
								}
							?>
						</a>
					</h1>
				</div><!-- .site-branding -->
			</div>
			<div class="uk-width-7-10 header-main-menu">
				<nav id="site-navigation" class="main-navigation uk-hidden-small">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'phdhub' ); ?></button>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
					) );
					?>
				</nav><!-- #site-navigation -->
			</div>
		</div>
	</header><!-- #masthead -->

	<div class="sticky-header" data-uk-sticky="{clsinactive: 'hidden-sticky', clsactive: 'uk-active', animation: 'uk-animation-slide-top'}">
		<div class="container">
		<div class="uk-grid">
			<div class="uk-width-1-10">
					<div class="sticky-menu" data-uk-dropdown>
						<div><span class="fa fa-bars" aria-hidden="true" title="Menu"></span></div>
						<div class="uk-dropdown">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-1',
								) );
							?>
						</div>
					</div>
			</div>
			<div class="uk-width-7-10">
				<div class="site-branding">
					<h1 class="site-title">
						<a href="<?php echo esc_url( site_url( '/' ) ); ?>">
							<?php
								if ( has_custom_logo() ) {
									the_custom_logo();
								}
								else {
							?>
							<img src="<?php echo get_template_directory_uri() . '/images/phdhub.png'; ?>" alt="PhD Hub">
							<?php
								}
							?>
						</a>
					</h1>
				</div><!-- .site-branding -->
				<div class="local-hubs-menu" data-uk-dropdown>
						<div><?php echo __('All Hubs', 'phdhub'); ?></div>
						<div class="uk-dropdown">
							<?php
								$subsites = get_sites();
								if ( ! empty ( $subsites ) ) {
									echo '<ul class="subsites">';
									foreach( $subsites as $subsite ) {
										$subsite_id = get_object_vars( $subsite )["blog_id"];
										$subsite_name = get_blog_details( $subsite_id )->blogname;
										$subsite_link = get_blog_details( $subsite_id )->siteurl;
										echo '<li class="site-' . $subsite_id . '"><a href="' . $subsite_link . '">' . $subsite_name . '</a></li>';
									}
									echo '</ul>';
								}
							?>
						</div>
					</div>
					<?php
						if ( is_active_sidebar('language-switcher') ) {
					?>
					<a class="language-switcher-link" href="#languages-list" data-uk-modal>
						<span class="fa fa-globe" aria-hidden="true"></span> <?php echo __('Select your language', 'phdhub'); ?>
					</a>
					<?php
						}
					?>
			</div>
			<div class="uk-width-2-10">
				<?php
					the_widget( 'phdhub_user_area' );
				?>
			</div>
		</div>
		</div>
	</div>
	<div id="content" class="site-content">
