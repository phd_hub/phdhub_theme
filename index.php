<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PhDHub
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
				if ( is_home() && is_front_page() ) {
					if (is_active_sidebar('showcase')) {
			?>
			<div id="showcase">
				<div class="container">
					<?php dynamic_sidebar('showcase'); ?>
				</div>
				<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 1920 184.8" style="enable-background:new 0 0 1920 184.8;margin: 0 0 -26px;" xml:space="preserve" preserveAspectRatio="none" class="svg hero__wave--svg replaced-svg">
					<style type="text/css">
						.st0{fill:#FFFFFF;}
					</style>

					<path class="st0" d="       
						M367,41.4       
						c235-43.3,518-74.9,736.8,23.9       
						c121.4,54.9,270.6,83.2,395.6,83.2       
						c76.1,-2,182.4-11,383.1-54.4         
						c13.4-7.9,55.9-15.3,37.5-52.2       
						v123       
						H0l0-172       
						C0,32.8,132,84.6,367,41.4       
						z"></path>
				</svg>
			</div>
			<?php
					}
					if (is_active_sidebar('top')) {
				?>
				<div id="top">
					<div class="container">
						<?php dynamic_sidebar('top'); ?>
					</div>
				</div>
				<?php
					}
					if (is_active_sidebar('main-top')) {
			?>
			<div id="main-top">
				<div class="container">
					<?php dynamic_sidebar('main-top'); ?>
				</div>
			</div>
			<?php
					}
					if (is_active_sidebar('main-top-2')) {
			?>
			<div id="main-top-2">
				<div class="container">
					<?php dynamic_sidebar('main-top-2'); ?>
				</div>
			</div>
			<?php
					}
					if (is_active_sidebar('main-top-3')) {
			?>
			<div id="main-top-3">
				<div class="container">
					<?php dynamic_sidebar('main-top-3'); ?>
				</div>
			</div>
			<?php
					}
					if (is_active_sidebar('main-bottom')) {
			?>
			<div id="main-bottom">
				<div class="container">
					<?php dynamic_sidebar('main-bottom'); ?>
				</div>
			</div>
			<?php
					}
				}
				else {
					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) :
							?>
							<header>
								<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
							</header>
							<?php
						endif;

						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							 * Include the Post-Type-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_type() );

						endwhile;

						the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
				}
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
