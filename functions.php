<?php
/**
 * PhDHub functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package PhDHub
 */

if ( ! function_exists( 'phdhub_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function phdhub_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on PhDHub, use a find and replace
		 * to change 'phdhub' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'phdhub', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'phdhub' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'phdhub_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'phdhub_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function phdhub_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'phdhub_content_width', 640 );
}
add_action( 'after_setup_theme', 'phdhub_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function phdhub_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'phdhub' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Archive Sidebar (PhD Openings)', 'phdhub' ),
		'id'            => 'phd-opening-archive-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Language Switcher', 'phdhub' ),
		'id'            => 'language-switcher',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'User Area', 'phdhub' ),
		'id'            => 'user-area',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Showcase', 'phdhub' ),
		'id'            => 'showcase',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Top', 'phdhub' ),
		'id'            => 'top',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Main Top', 'phdhub' ),
		'id'            => 'main-top',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Main Top 2', 'phdhub' ),
		'id'            => 'main-top-2',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Main Top 3', 'phdhub' ),
		'id'            => 'main-top-3',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Main Bottom', 'phdhub' ),
		'id'            => 'main-bottom',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Bottom', 'phdhub' ),
		'id'            => 'bottom',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'phdhub' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'phdhub' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'phdhub' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'phdhub' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 5', 'phdhub' ),
		'id'            => 'footer-5',
		'description'   => esc_html__( 'Add widgets here.', 'phdhub' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'phdhub_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function phdhub_scripts() {
	wp_enqueue_style( 'phdhub-font', 'https://fonts.googleapis.com/css?family=Encode+Sans+Semi+Expanded:300,400,500,700', false );
	wp_enqueue_style( 'phdhub-style', get_stylesheet_uri() );
	wp_enqueue_style( 'phdhub-uikit', get_template_directory_uri() . '/css/uikit.css' );
	wp_enqueue_style( 'phdhub-uikit-sticky', get_template_directory_uri() . '/css/sticky.min.css' );
	wp_enqueue_style( 'phdhub-css', get_template_directory_uri() . '/css/style.css' );

	wp_enqueue_script( 'phdhub-uikit', get_template_directory_uri() . '/js/uikit.min.js', array('jquery') );
	wp_enqueue_script( 'phdhub-uikit-grid', get_template_directory_uri() . '/js/grid.min.js', array('jquery') );
	wp_enqueue_script( 'phdhub-uikit-sticky', get_template_directory_uri() . '/js/sticky.min.js', array('jquery') );
	wp_enqueue_script( 'phdhub-uikit-modal', get_template_directory_uri() . '/js/modal.min.js', array('jquery') );
	wp_enqueue_script( 'phdhub-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'phdhub-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'phdhub_scripts' );


function phdhub_pages(){
	
	/*
	 * Contact Page
	 */
	$page = get_page_by_title('Contact Us', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
		$page_id = wp_insert_post(array(
			'post_title' => 'Contact Us',
			'post_type' => 'page',
			'post_name' => 'contact-us',
			'post_status' => 'publish',
			'post_excerpt' => 'Contact us page'
			));
	}
        add_post_meta($page_id, '_wp_page_template', 'contact-page.php');

	/*
	 * Partners Page
	 */
	$page = get_page_by_title('Partners', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
		$page_id = wp_insert_post(array(
			'post_title' => 'Partners',
			'post_type' => 'page',
			'post_name' => 'partners',
			'post_status' => 'publish',
			'post_excerpt' => 'Partners page',
			'post_content' => '[phdhub_partners]',
			));
	}
	
	/*
	 * Local Hubs Page
	 */
	$page = get_page_by_title('Local Hubs', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
		$page_id = wp_insert_post(array(
			'post_title' => 'Local Hubs',
			'post_type' => 'page',
			'post_name' => 'local-hubs',
			'post_status' => 'publish',
			'post_excerpt' => 'Local Hubs page',
			'post_content' => '[phdhub_local_hubs]',
			));
	}
	
	/*
	 * PhD Hub Resources Page
	 */
	$page = get_page_by_title('PhD Hub Resources', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
		$page_id = wp_insert_post(array(
			'post_title' => 'PhD Hub Resources',
			'post_type' => 'page',
			'post_name' => 'resources',
			'post_status' => 'publish',
			'post_excerpt' => 'PhD Hub Resources page',
			'post_content' => '<table style="width: 100%; border-collapse: collapse; background-color: #ffffff;" border="0">
<tbody>
<tr style="height: 23px;">
<th style="height: 23px; width: 52.1739%;"><strong>Filename</strong></th>
<th style="height: 23px; width: 25.4994%;"><strong>Date</strong></th>
<th style="height: 23px; width: 14.5711%;"><strong>Size</strong></th>
<th style="height: 23px; width: 7.63807%;"></th>
</tr>
<tr style="height: 27px;">
<td style="height: 27px; width: 52.1739%;">Visual Guidelines [PDF]</td>
<td style="height: 27px; width: 25.4994%;">27.02.2018</td>
<td style="height: 27px; width: 14.5711%;">314kb</td>
<td style="height: 27px; width: 7.63807%;"><a href="/wp-content/uploads/2019/03/PhDHub_VisualGuidelines.pdf"><i class="fa fa-download"></i></a></td>
</tr>
<tr style="height: 27px;">
<td style="height: 27px; width: 52.1739%;">PhD HUB Logo [PNG]</td>
<td style="height: 27px; width: 25.4994%;">27.02.2018</td>
<td style="height: 27px; width: 14.5711%;">107kb</td>
<td style="height: 27px; width: 7.63807%;"><a href="/wp-content/uploads/2019/03/phdhub-logo-transparent-bcg-3.png"><i class="fa fa-download"></i></a></td>
</tr>
</tbody>
</table>
<table style="width: 100%; border-collapse: collapse; background-color: #ffffff;" border="0">
<tbody>
<tr style="height: 27px;">
<td style="height: 27px; width: 52.1739%;">PhD Hub semantic annotation</td>
<td style="height: 27px; width: 25.4994%;">04.07.2018</td>
<td style="height: 27px; width: 14.5711%;">107kb</td>
<td style="height: 27px; width: 7.63807%;"><a href="/wp-content/uploads/2019/03/Semantic-Annotation_Linking_Classifications.pdf"><i class="fa fa-download"></i></a></td>
</tr>
</tbody>
</table>
<table style="width: 100%; border-collapse: collapse; background-color: #ffffff;" border="0">
<tbody>
<tr style="height: 27px;">
<td style="height: 27px; width: 52.1739%;">Guidelines on international cooperation in doctoral studies</td>
<td style="height: 27px; width: 25.4994%;">14.10.2018</td>
<td style="height: 27px; width: 14.5711%;">107kb</td>
<td style="height: 27px; width: 7.63807%;"><a href="/wp-content/uploads/2019/03/2.2-International-cooperation-guide.pdf"><i class="fa fa-download"></i></a></td>
</tr>
</tbody>
</table>',
			));
	}
	
	/*
	 * Login Page
	 */
	$page = get_page_by_title('Login', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
		$page_id = wp_insert_post(array(
			'post_title' => 'Login',
			'post_type' => 'page',
			'post_name' => 'login',
			'post_status' => 'publish',
			'post_excerpt' => 'User login page'
			));
	}
        add_post_meta($page_id, '_wp_page_template', 'login-page.php');

	/*
	 * Profile page
	*/
        $page = get_page_by_title('Profile', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
                $page_id = wp_insert_post(array(
                        'post_title' => 'Profile',
                        'post_type' => 'page',
                        'post_name' => 'profile',
                        'post_status' => 'publish',
                        'post_excerpt' => 'User profile page'
                        ));
        }
        add_post_meta($page_id, '_wp_page_template', 'profile-page.php');

	/*
	 *	Account Settings
	 */
        $page = get_page_by_title('Account Settings', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
                $page_id = wp_insert_post(array(
                        'post_title' => 'Account Settings',
                        'post_type' => 'page',
                        'post_name' => 'account-settings',
                        'post_status' => 'publish',
                        'post_excerpt' => 'Account Settings page'
                        ));
        }
        add_post_meta($page_id, '_wp_page_template', 'settings-page.php');
		
	/*
	 *	Password Settings
	 */
        $page = get_page_by_title('Password Settings', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
                $page_id = wp_insert_post(array(
                        'post_title' => 'Password Settings',
                        'post_type' => 'page',
                        'post_name' => 'password-settings',
                        'post_status' => 'publish',
                        'post_excerpt' => 'Password Settings page'
                        ));
        }
        add_post_meta($page_id, '_wp_page_template', 'password-settings-page.php');
		
		/*
		 * About page
		 */
        $page = get_page_by_title('About Us', OBJECT, 'page');
        $page_id = null == $page ? -1 : $page->ID;
        if($page_id == -1){
                $page_id = wp_insert_post(array(
                        'post_title' => 'About Us',
                        'post_type' => 'page',
                        'post_name' => 'about-us',
                        'post_status' => 'publish',
                        'post_excerpt' => 'About PhD Hub platform page',
						'post_content' => '<p style="text-align: center;">The PhD Hub is the single online resource for business-driven research. It connects universities, students and businesses at local and European level through an online platform for research and innovation. The PhD Hub is an EU funded project under Erasmus+ Key Action 2 - Knowledge Alliance.

</p>
<img class="size-medium aligncenter" src="/wp-content/uploads/2019/03/about_graph02.png" width="556" height="391" />

&nbsp;

&nbsp;
<p style="text-align: center;">The partners of the project are:</p>

<h3 id="project-coordinator" style="text-align: center;">Project Coordinator</h3>
<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
<h3 class="uk-accordion-title">European University Foundation</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/EUF.png" alt="" /></div>
<div class="uk-width-3-4">

The <a href="http://uni-foundation.eu/">European University Foundation</a> is a network of universities committed to bring about a modern, strong and competitive European Higher Education Area. The EUF is also an influential advocate for a substantial increase of the quantity and quality of student mobility, regularly putting forward new ideas, policies and recommendations. The network has a rich history of promoting policy reform and contributing to the further development of the Erasmus programme.
<a href="http://uni-foundation.eu"><strong>uni-foundation.eu</strong></a>

</div>
</div>
</div>
</div>
<h3 id="thessaloniki-hub" style="text-align: center;">Thessaloniki PhD Hub</h3>
<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
<h3 class="uk-accordion-title">Aristotle University of Thessaloniki</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/AUTH-460x235.png" alt="" /></div>
<div class="uk-width-3-4">

The Aristotle University of Thessaloniki (AUTh) is the largest university in Greece, with 11 Faculties organised into 41 Schools. Almost 73,000 under- and postgraduate students study at AUTh, while the Teaching and Research Staff numbers 2,500 people. AUTh holds 146 International Scientific Agreements with Universities all over the world, 538 bilateral agreements with European Universities.
<a href="https://www.auth.gr/en"><strong>www.auth.gr/en</strong></a>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Hellenic Petroleum S.A.</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/Hellenic-Petroleum.png" alt="" /></div>
<div class="uk-width-3-4">

Hellenic Petroleum S.A. is a dynamic Group with solid foundations, holding a leading position in the Greek energy sector as well as in the greater area of Southeast Europe. The Group’s range of activities include:
<ul>
 	<li>Supply, Refining and Trading of petroleum products, both in Greece and abroad;</li>
 	<li>Fuels Retail Market, both in Greece and abroad;</li>
 	<li>Petrochemicals/Chemicals Production and Trading;</li>
 	<li>Oil &amp; Gas Exploration and Production;</li>
 	<li>Electric Power Generation &amp; Trading;</li>
 	<li>Natural Gas Trading;</li>
 	<li>Renewable Energy Sources;</li>
 	<li>Provision of Consulting and Engineering services to hydrocarbon related projects;</li>
 	<li>Participation in the transportation of crude oil and products (pipelines, sea transportation).</li>
</ul>
<strong><a href="http://www.helpe.gr/en/">www.helpe.gr</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Euroconsultants S.A. (ECSA)</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4">  <img src="/wp-content/uploads/2019/03/euroconsultants.png" alt="" /></div>
<div class="uk-width-3-4">

EUROCONSULTANTS S.A. DEVELOPMENT &amp; TECHNOLOGY (ECSA) was established in 1990 and today is one of the leading consulting companies of Greece, with strong international orientation and presence. Euroconsultants brings practical solutions to public and private sector across the world in achieving development and growth targets in the sectors of Business &amp; SME Development, Industrial development and Restructuring, Energy &amp; Environment, Socio-Economic &amp; Regional Development, Government &amp; Society, Health, Information &amp; Communication Technology, Innovation &amp; Technology, Project &amp; Programme Management.

<a href="http://www.euroconsultants.gr/web/guest/aboutus"><strong>www.euroconsultants.gr</strong></a>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Associate Partners</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-1">
<div style="padding: 40px;"><strong>Alumil SA
</strong><strong><a href="http://www.alumil.com/en/">www.alumil.com</a></strong>
<strong>Municipality of Thessaloniki
<a href="https://thessaloniki.gr/?lang=en">thessaloniki.gr
</a></strong><strong>Open Knowledge Foundation
<a href="https://okfn.org/">okfn.org</a></strong><strong>
</strong></div>
</div>
</div>
</div>
</div>
<h3 id="alcala-hub" style="text-align: center;">Alcalá PhD Hub</h3>
<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
<h3 class="uk-accordion-title">University of Alcalá</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/Alcala.png" alt="" /></div>
<div class="uk-width-3-4">

The University of Alcalá (UAH), is an ancient and comprehensive University founded in the 16th century, with old and beautiful buildings and convents now used as Faculties. With a wide range of courses it provides an international and cosmopolitan atmosphere with many international students. It is located close to Madrid and its airport. The UAH is and has been involved in a wide variety of EU-funded projects, more particular for handling Erasmus mobilities of staff, teachers and students. The UAH has also led a strategic partnership, #europehome, aimed at bridging the gap between academia and business with a view to increase the relevance and entrepreneurial components of study programmes. The University is also leading an Erasmus+ capacity building project in building capacity and innovation in Latin-America.

<strong><a href="https://www.uah.es">www.uah.es</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Advantic Sistemas y Servicios</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/ADV-logo.png" alt="" /></div>
<div class="uk-width-3-4">

ADVANTICSYS is a high-tech SME, which has an important background in information and
communication technologies specialised in the field of Internet of Things (IOT) and other remote monitoring systems. Established in 2009, its main market niches are in the field of energy efficiency, environmental monitoring, and industrial processes automation. ADVANTICSYS has participated in several EU-funded projects within FP7 and H2020 programmes. Among them, research projects in the field of energy smart grids (INTrEPID,
ODYSSEUS) and environmental monitoring services (WESENSEIT, WISDOM) have been the
main topics where commercial products have been developed.

<strong><a href="http://www.advanticsys.com/about-us/">www.advanticsys.com</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Associate Partners</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-1">
<div style="padding: 40px;"><strong>Indra Sistemas SA</strong>
<strong><a href="http://www.indracompany.com/en">www.indracompany.com</a></strong>
<strong>Plain Concepts
<a href="http://www.plainconcepts.com">www.plainconcepts.com</a></strong></div>
</div>
</div>
</div>
</div>
<h3 id="birmingham-hub" style="text-align: center;">Birmingham PhD Hub</h3>
<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
<h3 class="uk-accordion-title">Birmingham City University (BCU)</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/BCU-colour-logo.jpg" alt="" /></div>
<div class="uk-width-3-4">

It is the second largest of the three universities in the city of Birmingham, the second largest city of the United Kingdom. BCU currently has 23,500 students from 80 countries, and 20% of them are postgraduate students. The University is diverse and an increasing popular place to study and to undertake research. The University places students high in its activities, providing them with excellent opportunities for future success. It is also actively promoting research with a balance of theory and practice in key areas, including computer science, through the appointment and support of research-active personnel.

<strong><a href="https://www.bcu.ac.uk">www.bcu.ac.uk</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Microsoft Research</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/Microsoft-Research-logo.jpg" alt="" /></div>
<div class="uk-width-3-4">

In the UK, we share Microsoft’s global vision to empower every person and organisation to achieve more; but we also pursue a wide range of initiatives with particular resonance and
relevance to the UK. To realize that mission requires more than providing products and services that let our customers do great things. It requires us to be thoughtful about the impact of our own business practices, policies, and investments in communities as well as partnerships to apply our technologies to address some of the world’s toughest challenges.

From working responsibly and sustainably to ensuring that the technology we create makes life better – not just for our customers, partners and employees, but for everyone. We are empowering the education community with a wide range of products, services and programmes that help transform learning and make it more accessible.

<strong><a href="https://www.microsoft.com/en-us/research/">www.microsoft.com</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Associate Partners</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-1">
<div style="padding: 40px;"><strong>Deep-Secure Limited</strong>
<strong><a href="http://www.deep-secure.com">www.deep-secure.com</a></strong>
<strong>Titan Partnership</strong>
<strong><a href="http://www.titan.org.uk">www.titan.org.uk</a></strong></div>
</div>
</div>
</div>
</div>
<h3 id="lodz-hub" style="text-align: center;">Łódź PhD Hub</h3>
<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
<h3 class="uk-accordion-title">University of Łódź (ULO)</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/ULO-logo.png" alt="" /></div>
<div class="uk-width-3-4">

The University of Łódź (ULO), established in 1945, is one of the leading institutions of higher education in Poland. The ULO is a large academic centre, actively participating in EU programmes, such as LLP, Culture, Jean Monnet, Erasmus Mundus, Framework Programmes, Erasmus+, etc. It cooperates internationally with 204 partner institutions all over the world. The centre for Technology Transfer at ULO is an innovative unit established under the US-Polish Offset Programme as a part of the Faculty of Management, with almost 12-year experience in various national and international initiatives related to technology transfer and commercialisation of R&amp;D results.

<strong><a href="https://iso.uni.lodz.pl/">iso.uni.lodz.pl</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">LSI Software S.A.</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-4"><img src="/wp-content/uploads/2019/03/LSI-software-400px.png" alt="" /></div>
<div class="uk-width-3-4">

LSI Software S.A. is a leading Polish software developer for retail and hospitality applications.
The abbreviation of the first part of the company name stands for Large Scale Innovations, signifying the effort to achieve the highest possible level of innovation and excellence of the supplied IT solutions. Our quest for innovation is combined with an understanding of the customers’ needs and the environment in which they function. LSI Software seeks people with potential. We offer conditions for quick self-development. Professional experience is not a required precondition. We put the emphasis on knowledge gathered during university studies and potential. The comprehensive offering of LSI Software includes software, professional services, implementation services, maintenance, and the supplying of expert hardware solutions. The main asset of the company is the extensive knowledge of the rules of functioning in the retail and hospitality sectors.

<strong><a href="http://www.lsisoftware.pl">www.lsisoftware.pl</a></strong>

</div>
</div>
</div>
<h3 class="uk-accordion-title">Associate Partners</h3>
<div class="uk-accordion-content">
<div class="uk-grid">
<div class="uk-width-1-1">
<div style="padding: 40px;"><strong>Lodzkie Region</strong>
<strong><a href="https://www.lodzkie.pl">www.lodzkie.pl</a></strong>
<strong>StartMoney</strong>
<strong><a href="http://startmoney.pl">startmoney.pl</a></strong></div>
</div>
</div>
</div>
</div>
<div id="other-partners-section">
<h3 id="other-partners" style="text-align: center;">Other Strategic Partners</h3>
<strong>Eurodoc</strong>
<strong><a href="http://www.eurodoc.net/">www.eurodoc.net</a></strong>

<strong>Technoport SA</strong>
<strong><a href="http://www.technoport.lu">www.technoport.lu</a></strong>

<strong>Lomonosov State University
<a href="https://www.msu.ru/en">www.msu.ru</a></strong>

<strong>University of Novi Sad
<a href="https://www.uns.ac.rs">www.uns.ac.rs</a></strong>

</div>'
                        ));
        }
		
	$menu_name = 'PhD Hub Menu';
	$menu_exists = wp_get_nav_menu_object($menu_name);
	$menu_id = wp_create_nav_menu($menu_name);
	if ( !$menu_exists) {
		
		wp_update_nav_menu_item($menu_id, 0, array(
			'menu-item-title' => __('Home'),
			'menu-item-classes' => 'uk-hidden',
			'menu-item-url' => home_url('/'),
			'menu-item-status' => 'publish')
		);
			
		wp_update_nav_menu_item($menu_id, 0, array(
			'menu-item-title' => __('PhD Offers'),
			'menu-item-url' => site_url() . '/phd-openings',
			'menu-item-status' => 'publish',
			'menu-item-type' => 'custom')
		);
			
		wp_update_nav_menu_item($menu_id, 0, array(
			'menu-item-title' => __('Find your Partners'),
			'menu-item-url' => site_url() . '/cooperation-calls',
			'menu-item-status' => 'publish',
			'menu-item-type' => 'custom')
		);
			
		wp_update_nav_menu_item($menu_id, 0, array(
			'menu-item-title' => __('Our Blog'),
			'menu-item-url' => site_url() . '/category/our-blog',
			'menu-item-status' => 'publish',
			'menu-item-type' => 'custom')
		);
		
		wp_update_nav_menu_item($menu_id, 0, array(
			'menu-item-title' => __('About Us'),
			'menu-item-url' => site_url() . '/about-us',
			'menu-item-status' => 'publish',
			'menu-item-type' => 'custom')
		);
	}
}
//add_action( 'init', 'phdhub_pages'); 

add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host       = SMTP_HOST;
    $phpmailer->SMTPAuth   = SMTP_AUTH;
    $phpmailer->Port       = SMTP_PORT;
    $phpmailer->SMTPSecure = SMTP_SECURE;
    $phpmailer->Username   = SMTP_USERNAME;
    $phpmailer->Password   = SMTP_PASSWORD;
    $phpmailer->From       = SMTP_FROM;
    $phpmailer->FromName   = SMTP_FROMNAME;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 * Remove "Category:", "Tag:", "Author:" from their titles
 */
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } else if ( is_post_type_archive() ) {

            $title = post_type_archive_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

remove_action('wp_head', 'wp_generator');


/*
 * Add default widgets to theme's sidebars
 */
//add_action( 'widgets_init', 'phdhub_theme_widgets' );  
function phdhub_theme_widgets() {   
    $active_widgets = get_option( 'sidebars_widgets' ); 
 
	/*
	 * Add PhD Openings Filters in Archive Sidebar if it is not empty
	 */
	if ( !empty ( $active_widgets[ 'phd-opening-archive-sidebar' ] ) || !empty ( $active_widgets['language-switcher'] ) || 
		!empty ( $active_widgets['user-area'] ) || !empty ( $active_widgets['showcase'] ) || !empty ( $active_widgets['top'] ) || !empty ( $active_widgets['main-top'] ) ||
		!empty ( $active_widgets['main-bottom'] ) || !empty ( $active_widgets['footer-1'] ) || !empty ( $active_widgets['footer-2'] ) || !empty ( $active_widgets['footer-3'] ) ||
		!empty ( $active_widgets['footer-4'] ) || !empty ( $active_widgets['footer-5'] ) ) {
		return;
	}
	
	$counter = 1;
	
	$active_widgets[ 'phd-opening-archive-sidebar' ][0] = 'phd_openings_filters-' . $counter;
	
	$filters_content[ $counter ] = array (
		'style' => 'advanced',
	);
	
	
	/*
	 * Add Language Switcher if sidebar is not empty
	 */
	
	$counter++;
	
	$active_widgets[ 'language-switcher' ][] = 'polylang-' . $counter;
	
	$lang_content[ $counter ] = array (
		'show_names' => 1,
		'show_flags' => 1,
	);
	update_option( 'widget_polylang', $lang_content );
	
	
	/*
	 * Add PhD Hub - User Area widget if sidebar is not empty
	 */
	if ( !empty ( $active_widgets[ 'user-area' ] ) ) {
		return;
	}
	
	$counter++;
	
	$active_widgets[ 'user-area' ][] = 'phdhub_user_area-' . $counter;
	$user_content[ $counter ] = array (
		'lang_choice' => '0',
	);
	update_option( 'widget_phdhub_user_area', $user_content );
	
	
	/*
	 * Add Text and Search widgets if showcase sidebar is not empty
	 */
	
	$counter++;
	
	$active_widgets[ 'showcase' ][] = 'custom_html-' . $counter;
	$text_content[ $counter ] = array (
		'title' => 'Welcome to the PhD Hub platform',
		'content' => '<p>Browse thousands of business-driven PhD Offers worldwide.<br /><a href="cooperation-calls">Explore your opportunities now. Find your partners.</a></p>'
	);
	
	$counter++;
	
	$active_widgets[ 'showcase' ][] = 'phd_openings_filters-' . $counter;
	$filters_content[ $counter ] = array (
		'style' => 'advanced-homepage',
	);
	update_option( 'widget_phd_openings_filters', $filters_content );
	
	
	/*
	 * Add PhD Hub - User Area widget if sidebar is not empty
	 */
	
	$counter++;
	
	$active_widgets[ 'top' ][0] = 'phdhub_counter-' . $counter;
	$counter_content[ $counter ] = array (
		'lang_choice' => '0',
	);
	update_option( 'widget_phdhub_counter', $counter_content );
	
	
	/*
	 * Add PhD Hub - User Area widget if sidebar is not empty
	 */
	
	$counter++;
	
	$active_widgets[ 'main-top' ][0] = 'phdhub_phd_openings-' . $counter;
	$openings_content[ $counter ] = array (
		'title' => 'Recent PhD Offers',
		'number_of_openings' => '12',
		'style' => 'slider-three-columns',
		'lang_choice' => 'en',
		'widget_description' => ''
	);
	update_option( 'widget_phdhub_phd_openings', $openings_content );
	
	/*
	 * Add Custom HTML widget if sidebar (Main Top 2) is not empty
	 */
    $counter++; 
  
    $active_widgets[ 'main-top-2' ][0] = 'custom_html-' . $counter;  
   
    $text_content[ $counter ] = array (  
        'title'        => 'The single online resource for applied research',  
        'content'         => '<p style="text-align: center">
	The PhD Hub is the single online resource for business-driven research. It connects universities, students and businesses at local and European level through an online platform for research and innovation. The PhD Hub is an EU funded project under Erasmus+ Key Action 2 – Knowledge Alliance.
</p>
<div class="uk-grid">
	<div class="uk-width-1-3">
		<div class="box-content">
			<h4><strong>Establishing a quality framework for university-business cooperation</strong>
			</h4>
			<p>
				Each University-Business tandem will establish their local PhD Hub strategy, aimed at representing the interest of both academic and corporate fields and finding a joint approach for triggering research and ensuring its transferability to the society at large. 
			</p>
		</div>
	</div>
	<div class="uk-width-1-3">
		<div class="box-content">
			<h4><strong>Building an online infrastructure to increase research opportunities and their transferability</strong>
			</h4>
			<p>
				The local PhD Hubs will share information both at local and European level thanks to an innovative online platform connecting the local IT platforms among themselves.
			</p>
		</div>
	</div>
	<div class="uk-width-1-3">
		<div class="box-content">
			<h4><strong>Applying and mainstreaming the European PhD Hub</strong>
			</h4>
			<p>
				The University-Business tandems will be responsible for implementing their local PhD Hub strategy and informing/training both academic and corporate staff members and potential PhD students on the opportunities offered by the European PhD Hub.
			</p>
		</div>
	</div>
</div>',  
    );  
    
    
	/*
	 * Add Custom HTML widget if sidebar (Main Top 2) is not empty
	 */
    $counter++; 
  
    $active_widgets[ 'main-top-3' ][0] = 'custom_html-' . $counter;  
   
    $text_content[ $counter ] = array (  
        'title'        => 'Join the Hub',  
        'content'         => '<p style="text-align: center">
	The PhD Hub implements a coordination mechanism triangulating the needs of businesses, universities and students at both local and European level. This will trigger new funding opportunities since calls for cooperation both from university and corporate partners will allow to match research needs and allow for public/private partnerships in research activities.
</p>
<div class="uk-grid">
	<div class="uk-width-1-3">
		<div class="box-content">
			<img src="/wp-content/uploads/2019/03/universities-homepage.png" alt="">
			<h4><strong>Universities</strong>
			</h4>
			<p>
				Universities have set as their mission to contribute to the knowledge of our societies by carrying out fundamental and applied research activities. With the increased availability of funding and clearer perspectives for research results, it is also expected that students will be more likely to apply for PhD studies, therefore enabling universities to recruit from a larger group of students and to attract the best talents needed for the knowledge-based society.
		<a href="https://phdhub.eu/wp-login.php">Sign In</a>
			</p>
		</div>
	</div>
	<div class="uk-width-1-3">
		<div class="box-content">
			<img src="/wp-content/uploads/2019/03/businesses-homepage.png" alt="">
			<h4><strong>Businesses</strong>
			</h4>
			<p>
				Businesses need innovation within their market to remain competitive and face a globalised competition. This is even more important for SMEs which do not necessarily have the capacity of creating their own research department and therefore rely on external support. It appears that SMEs are also not aware of the knowledge, skills and potential asset of PhD holders, hence a structured cooperation can create a great amount of opportunities in this field.
				<a href="https://phdhub.eu/wp-login.php">Sign In</a>
			</p>
		</div>
	</div>
	<div class="uk-width-1-3">
		<div class="box-content">
			<img src="/wp-content/uploads/2019/03/students-homepage.png" alt="">
			<h4><strong>PhD Students</strong>
			</h4>
			<p>
				Phd students also stand to benefit since their contacts with local and international business partners will increase through the European PhD Hub – this will provide them with the opportunity to perform placements abroad and carry out research activities for companies, therefore solving a practical challenge a given company is facing.
				<a href="https://phdhub.eu/wp-login.php">Sign In</a>
			</p>
		</div>
	</div>
</div>',  
    );  
	
	/*
	 * Add PhD Hub - User Area widget if sidebar is not empty
	 */
	if ( !empty ( $active_widgets[ 'main-bottom' ] ) ) {
		return;
	}
	
	$counter++;
	
	$active_widgets[ 'main-bottom' ][0] = 'jsquare_posts_showcase-' . $counter;
	$jtposts_content[ $counter ] = array (
		'title' => 'From Our Blog',
		'style' => 'grid-four-columns',
		'show_more' => 'no',
		'source' => 'wp-posts',
		'posts_number' => '4',
		'post_text_chars' => '100',
		'show_date' => 'yes',
		'show_comments' => 'yes',
		'show_more_text' => '',
		'show_category' => 'yes',
		'post_category' => 'all',
		'include_post_types' => '',
		'orderby' => 'id',
		'order' => 'DESC'
	);
	update_option( 'widget_jsquare_posts_showcase', $jtposts_content );
	
	
	/*
	 * Add Custom HTML widget if sidebar (Footer 1) is not empty
	 */
    $counter++; 
  
    $active_widgets[ 'footer-1' ][0] = 'custom_html-' . $counter;  
   
    $text_content[ $counter ] = array (  
        'title'        => 'PhD Hub',  
        'content'         => '<ul>
	<li><a href="/phd-openings">PhD Offers</a></li>
	<li><a href="/cooperation-calls">Find your partners</a></li>
</ul>',  
    );  
	
	
	/*
	 * Add Custom HTML widget if sidebar (Footer 2) is not empty
	 */
	$counter++;
	
	$active_widgets[ 'footer-2' ][0] = 'custom_html-' . $counter;
	
	$text_content[ $counter ] = array (
		'title' => 'About PhD Hub',
		'content' => '<ul>
	<li><a href="/about-us">About Us</a></li>
	<li><a href="/category/our-blog/">Our Blog</a></li>
	<li><a href="/resources">PhD Hub Resources</a></li>
	<li><a href="/contact-us">Contact Us</a></li>
</ul>',
	);
	
	
	/*
	 * Add Custom HTML widget if sidebar (Footer 3) is not empty
	 */
	
	$counter++;
	
	$active_widgets[ 'footer-3' ][0] = 'custom_html-' . $counter;
	
	$text_content[ $counter ] = array (
		'title' => 'Pages',
		'content' => '<ul>
	<li><a href="/local-hubs">Local Hubs</a></li>
</ul>',
	);
	
	/*
	 * Add Custom HTML widget if sidebar (Footer 4) is not empty
	 */
	 
	$counter++;
	
	$active_widgets[ 'footer-4' ][0] = 'custom_html-' . $counter;
	
	$text_content[ $counter ] = array (
		'title' => 'Privacy & Security',
		'content' => '<ul>
	<li><a href="#">Privacy Policy</a></li>
	<li><a href="#">Cookies</a></li>
	<li><a href="#">Terms of Use</a></li>
</ul>',
	);
	
	/*
	 * Add Custom HTML widget if sidebar (Footer 5) is not empty
	 */
	
	$counter++;
	
	$active_widgets[ 'footer-5' ][0] = 'custom_html-' . $counter;
	
	$text_content[ $counter ] = array (
		'title' => 'Follow us',
		'content' => '<ul>
	<li><a href="#">Twitter</a></li>
	<li><a href="#">LinkedIn</a></li>
</ul>',
	);
	update_option( 'widget_custom_html', $text_content );
	
	
	/*
	 * Update all sidebars
	 */
    update_option( 'sidebars_widgets', $active_widgets );  
}  


/*
 * Remove Theme Customizer Settings
 */
 function phdhub_theme_remove_customizer_settings() {
	global $wp_customize;
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'static_front_page' );
 }
 add_action( 'customize_register', 'phdhub_theme_remove_customizer_settings' );
 
 
 /*
  * Customize Login Page
  */
function my_login_logo() { 
?>
	<style type="text/css">
		body.login {
			background: #f1f1f1 url(<?php echo get_stylesheet_directory_uri(); ?>/images/showcaseBG.png) center top no-repeat;
			background-size: cover;
		}
		#login {
			background: rgba(255, 255, 255, 0.92);
			margin: 0 !important;
			padding: 20px 0 40px !important;
			width: 100% !important;
		}
		#loginform {
			margin: 0 auto;
			width: 300px;
		}
		#nav, #backtoblog {
			text-align: center;
		}
		#login h1 a, .login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/phdhub-logo.png);
			background-repeat: no-repeat;
			padding-bottom: 30px;
			background-size: 350px;
			height: 120px;
			width: 100%;
		}
		.heateor_ss_sl_optin_container {
			margin: 0 0 15px !important;
		}
		.login .message {
			margin: 0 auto 30px !important;
			width: 320px !important;
		}
	</style>
<?php 
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );



function phdhub_google_analytics() {
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-73941539-4"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-73941539-4');
</script>
<?php
}
add_action('wp_head', 'phdhub_google_analytics');

function phdhub_add_users_to_local_hubs() {
	$blogusers = get_users( array( 'blog_id' => 0, 'fields' => array('ID','user_email' ) ) );
        foreach ( $blogusers as $user ) {
		if (preg_match('|auth.gr$|', $user->user_email)){
			$blog_id = '5';
                        $user_id = $user->ID;
                        $role = 'subscriber';

                        if (! is_user_member_of_blog( $user_id, $blog_id )) {
                        	add_user_to_blog($blog_id, $user_id, $role);
                        }
                }
		elseif (preg_match('|bcu.ac.uk$|', $user->user_email)) {
			$blog_id = '4';
			$user_id = $user->ID;
			$role = 'subscriber';

			if (! is_user_member_of_blog( $user_id, $blog_id )) {
				add_user_to_blog($blog_id, $user_id, $role);
			}
		}
		elseif (preg_match('|uni.lodz.pl$|', $user->user_email)) {
                        $blog_id = '3';
                        $user_id = $user->ID;
                        $role = 'subscriber';

                        if (! is_user_member_of_blog( $user_id, $blog_id )) {
                                add_user_to_blog($blog_id, $user_id, $role);
                        }
                } 
		elseif (preg_match('|uah.es$|', $user->user_email)) {
                        $blog_id = '2';
                        $user_id = $user->ID;
                        $role = 'subscriber';

                        if (! is_user_member_of_blog( $user_id, $blog_id )) {
                                add_user_to_blog($blog_id, $user_id, $role);
                        }
                }
	}
}
add_action('init', 'phdhub_add_users_to_local_hubs');

add_filter('coauthors_supported_post_types', function( $post_types ) {
	//$post_types[] = array('companies', 'post', 'phd-openings', 'page', 'institutions', 'faculties', 'phd-programs', 'research-teams', 'cooperation-calls');
	$post_types[] = 'companies';
	$post_types[] = 'post';
        $post_types[] = 'page';
        $post_types[] = 'phd-openings';
        $post_types[] = 'institutions';
        $post_types[] = 'faculties';
        $post_types[] = 'phd-programs';
        $post_types[] = 'research-teams';
        $post_types[] = 'cooperation-calls';
	return $post_types;
});


function phdhub_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'phdhub_login_logo_url' );



function phdhub_getPostViews($postID){
	
	if ( is_singular('phd-openings') ) {
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
			return "0";
		}
		return $count;
	}
    
	if ( is_singular('cooperation-calls') ) {
		$count_key = 'calls_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
			return "0";
		}
		return $count;
	}
}
function phdhub_setPostViews($postID) {
	if ( is_singular('phd-openings') ) {
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}
	if ( is_singular('cooperation-calls') ) {
		$count_key = 'calls_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}
}
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


// Add the custom columns to the PhD Offers post type:
add_filter( 'manage_phd-openings_posts_columns', 'phdhub_offers_views_column' );
function phdhub_offers_views_column($columns) {
    unset($columns['date']);
    $columns['views'] = __( 'Views', 'phdhub' );
    $columns['date'] = 'Date';

    return $columns;
}

// Add the data to the custom columns for the PhD Offers post type:
add_action( 'manage_phd-openings_posts_custom_column' , 'phdhub_offers_views_custom_column', 10, 2 );
function phdhub_offers_views_custom_column( $column, $post_id ) {
    switch ( $column ) {

        case 'views' :
            $views = get_post_meta($post_id, 'post_views_count', true);
            if ( !empty ( $views ) )
                echo $views;
            else
                echo '0';
            break;

    }
}

// Add the custom columns to the PhD Offers post type:
add_filter( 'manage_cooperation-calls_posts_columns', 'phdhub_calls_views_column' );
function phdhub_calls_views_column($columns) {
    unset($columns['date']);
    $columns['views'] = __( 'Views', 'phdhub' );
    $columns['date'] = 'Date';

    return $columns;
}

// Add the data to the custom columns for the PhD Offers post type:
add_action( 'manage_cooperation-calls_posts_custom_column' , 'phdhub_calls_views_custom_column', 10, 2 );
function phdhub_calls_views_custom_column( $column, $post_id ) {
    switch ( $column ) {

        case 'views' :
            $views = get_post_meta($post_id, 'calls_views_count', true);
            if ( !empty ( $views ) )
                echo $views;
            else
                echo '0';
            break;

    }
}