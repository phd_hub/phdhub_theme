<?php
/**
 * PhDHub Theme Customizer
 *
 * @package PhDHub
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function phdhub_customize_register( $wp_customize ) {
	$wp_customize->add_setting( 'top_header_background' , array(
		'default'   => '#ffffff',
		'transport' => 'refresh',
	) );
	$wp_customize->add_setting( 'top_header_textcolor' , array(
		'default'   => '#262626',
		'transport' => 'refresh',
	) );
	$wp_customize->add_setting( 'top_header_link_color' , array(
		'default'   => '#494949',
		'transport' => 'refresh',
	) );
	$wp_customize->add_setting( 'top_header_link_hover_color' , array(
		'default'   => '#318fe8',
		'transport' => 'refresh',
	) );
	$wp_customize->add_setting( 'header_background' , array(
		'default'   => '#ffffff',
		'transport' => 'refresh',
	) );
	$wp_customize->add_setting( 'nav_link_color' , array(
		'default'   => '#323232',
		'transport' => 'refresh',
	) );
	$wp_customize->add_setting( 'nav_link_hover_color' , array(
		'default'   => '#1d72b6',
		'transport' => 'refresh',
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'top_header_background', array(
		'label'      => __( 'Top Header Background Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'top_header_background',
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'top_header_textcolor', array(
		'label'      => __( 'Top Header Text Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'top_header_textcolor',
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'top_header_link_color', array(
		'label'      => __( 'Top Header Link Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'top_header_link_color',
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'top_header_link_hover_color', array(
		'label'      => __( 'Top Header Link Hover Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'top_header_link_hover_color',
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_background', array(
		'label'      => __( 'Header Background Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'header_background',
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'nav_link_color', array(
		'label'      => __( 'Navigation Link Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'nav_link_color',
	) ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'nav_link_hover_color', array(
		'label'      => __( 'Navigation Link Hover Color', 'phdhub' ),
		'section'    => 'colors',
		'settings'   => 'nav_link_hover_color',
	) ) );
	
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'top_header_textcolor' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'top_header_link_color' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'top_header_link_hover_color' )->transport  = 'postMessage';
	$wp_customize->remove_control('header_textcolor');
	$wp_customize->get_setting( 'top_header_background' )->transport = 'postMessage';
	$wp_customize->get_setting( 'header_background' )->transport = 'postMessage';
	$wp_customize->get_setting( 'nav_link_color' )->transport = 'postMessage';
	$wp_customize->get_setting( 'nav_link_hover_color' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'phdhub_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'phdhub_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'phdhub_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function phdhub_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function phdhub_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function phdhub_customize_preview_js() {
	wp_enqueue_script( 'phdhub-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'phdhub_customize_preview_js' );
