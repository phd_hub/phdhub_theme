<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package PhDHub
 */

add_action( 'wp_head', 'phdhub_customizer_css');
function phdhub_customizer_css()
{
    ?>
         <style type="text/css">
             .site-header #top-header { 
				 background: <?php echo get_theme_mod('top_header_background', '#ffffff'); ?>; 
				 color: <?php echo get_theme_mod('top_header_textcolor', '#262626'); ?>;
			 }
             .site-header #top-header a { color: <?php echo get_theme_mod('top_header_link_color', '#494949'); ?>; }
             .site-header #top-header a:hover { color: <?php echo get_theme_mod('top_header_link_hover_color', '#318fe8'); ?>; }
             .site-header { background: <?php echo get_theme_mod('header_background', '#ffffff'); ?>; }
             .site-header .main-navigation a { color: <?php echo get_theme_mod('nav_link_color', '#323232'); ?>; }
             .site-header .main-navigation a:hover { color: <?php echo get_theme_mod('nav_link_hover_color', '#1d72b6'); ?>; }
         </style>
    <?php
}
/**
 * Set up the WordPress core custom header feature.
 *
 * @uses phdhub_header_style()
 */
function phdhub_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'phdhub_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
		'wp-head-callback'       => 'phdhub_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'phdhub_custom_header_setup' );

if ( ! function_exists( 'phdhub_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see phdhub_custom_header_setup().
	 */
	function phdhub_header_style() {
		$header_text_color = get_header_textcolor();

		/*
		 * If no custom options for text are set, let's bail.
		 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: add_theme_support( 'custom-header' ).
		 */
		if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
			return;
		}

		// If we get this far, we have custom styles. Let's do this.
		?>
		<style type="text/css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
			?>
			.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}
		<?php
		// If the user has set a custom color for the text use that.
		else :
			?>
			.site-title a,
			.site-description {
				color: #<?php echo esc_attr( $header_text_color ); ?>;
			}
		<?php endif; ?>
		</style>
		<?php
	}
endif;
