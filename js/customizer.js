/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title, .site-description' ).css( {
					'clip': 'auto',
					'position': 'relative'
				} );
				$( '.site-title a, .site-description' ).css( {
					'color': to
				} );
			}
		} );
	} );

	// Top header text color.
	wp.customize( 'top_header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header #top-header' ).css();
			} else {
				$( '.site-header #top-header' ).css('color', to );
			}
		} );
	} );

	// Top header link color.
	wp.customize( 'top_header_link_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header #top-header a' ).css();
			} else {
				$( '.site-header #top-header a' ).css('color', to );
			}
		} );
	} );

	// Top header link color.
	wp.customize( 'top_header_link_hover_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header #top-header a:hover' ).css();
			} else {
				$( '.site-header #top-header a:hover' ).css('color', to );
			}
		} );
	} );

	// Top header background color.
	wp.customize( 'top_header_background', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header #top-header' ).css();
			} else {
				$( '.site-header #top-header' ).css('background', to );
			}
		} );
	} );

	// Header background color.
	wp.customize( 'header_background', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header' ).css();
			} else {
				$( '.site-header' ).css('background', to );
			}
		} );
	} );

	// Navigation link color.
	wp.customize( 'nav_link_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header .main-navigation a' ).css();
			} else {
				$( '.site-header .main-navigation a' ).css('color', to );
			}
		} );
	} );

	// Navigation link hover color.
	wp.customize( 'nav_link_hover_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-header .main-navigation a:hover' ).css();
			} else {
				$( '.site-header .main-navigation a:hover' ).css('color', to );
			}
		} );
	} );
} )( jQuery );
