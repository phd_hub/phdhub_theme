<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package PhDHub
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<div class="container">
					<h1 class="page-title">
						<?php
						/* translators: %s: search query. */
						printf( esc_html__( 'Search Results for: %s', 'phdhub' ), '<span>' . get_search_query() . '</span>' );
						?>
					</h1>
					<?php get_search_form(); ?>
				</div>
			</header><!-- .entry-header -->

			<div class="page-content">
				<div class="uk-grid page-content-grid">
					<div class="uk-width-7-10">
						<div class="uk-grid search-results-items">
							<?php
								/* Start the Loop */
								while ( have_posts() ) :
									the_post();

									/**
									 * Run the loop for the search to output the results.
									 * If you want to overload this in a child theme then include a file
									 * called content-search.php and that will be used instead.
									 */
									get_template_part( 'template-parts/content', 'search' );

								endwhile;
							?>
						</div>
						<?php
							the_posts_navigation();
						?>
					</div>
					<div class="uk-width-3-10">
						<?php
							get_sidebar();
						?>
					</div>
				</div>
			</div>
			<?php

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
