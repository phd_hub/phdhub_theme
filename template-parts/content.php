<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PhDHub
 */

?>

<?php
	if ( is_singular() ) {
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				phdhub_posted_on();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php phdhub_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'phdhub' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'phdhub' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php phdhub_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
<?php
	}
	else {
?>
<div class="uk-width-1-4 post-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php phdhub_post_thumbnail(); ?>
		<header class="entry-header">
			<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;
			?>
		</header><!-- .entry-header -->


		<div class="entry-content">
			<?php
				echo substr(strip_tags(get_the_content()), 0, 120) . '...';
			?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<span class="date"><span class="fa fa-calendar"></span> <?php echo get_the_date('d F Y'); ?></span>
			<span class="comments"><span class="fa fa-comments-o"></span> <?php echo get_comments_number(); ?></span>
			<?php
		  		$categories = get_the_category();
				if ( ! empty( $categories ) ) {
			?>
			<span class="category">
				<span class="fa fa-folder-o"></span> <a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>"><?php echo esc_html( $categories[0]->name ); ?></a>
			</span>
			<?php
				}
		 	?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
<?php
	}
?>
