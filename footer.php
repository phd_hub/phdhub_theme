<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PhDHub
 */

?>

	</div><!-- #content -->
	
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"  id="footer-svg"><path fill="#fff" fill-opacity="1" d="M0,96L40,106.7C80,117,160,139,240,133.3C320,128,400,96,480,96C560,96,640,128,720,138.7C800,149,880,139,960,144C1040,149,1120,171,1200,176C1280,181,1360,171,1400,165.3L1440,160L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path></svg>
	<footer id="colophon" class="site-footer">
	
		<div class="uk-grid">
			<?php
				if ( is_active_sidebar( 'footer-1' ) ) {
			?>
			<div class="uk-width-1-4">
				<?php dynamic_sidebar( 'footer-1' ); ?>
			</div>
			<?php
				}
				if ( is_active_sidebar( 'footer-2' ) ) {
			?>
			<div class="uk-width-1-4">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
			<?php
				}
				if ( is_active_sidebar( 'footer-3' ) ) {
			?>
			<div class="uk-width-1-4">
				<?php dynamic_sidebar( 'footer-3' ); ?>
			</div>
			<?php
				}
				if ( is_active_sidebar( 'footer-4' ) ) {
			?>
			<div class="uk-width-1-4">
				<?php dynamic_sidebar( 'footer-4' ); ?>
			</div>
			<?php
				}
			?>
		</div>
		<div class="site-info">
			<div class="container">
				<div class="uk-grid">
					<div class="uk-width-1-2">
						<?php echo __('Copyright', 'phdhub') . ' &copy;  2019 <a href="' . site_url() . '">PhD Hub</a>. All rights reserved.'; ?>
					</div>
					<div class="uk-width-1-2">
						<img src="<?php echo get_template_directory_uri() . '/images/eu-funded-logo.png'; ?>" alt="Co-funded by the Erasmus+ Programme of the European Union">
					</div>
				</div>
				<p class="disclaimer"><?php echo __('The European Commission support for the production of this project does not constitute an endorsement of the contents which reflects the views only of the authors, and the Commission cannot be held responsible for any use which may be made of the information contained therein.', 'phdhub'); ?></p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
